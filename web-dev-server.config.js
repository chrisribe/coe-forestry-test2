module.exports = {
    open: "schools/index.html",
    watch: true,
    rootDir: "site_root",
    plugins: [
        {
            name: 'coe-components',
            transform(context) {
                let transformedBody = context.body.replace(
                    '<script type="module" src="/components.js', 
                    '<script type="module" src="https://web21.ufcoe.io/components.js'
                );
                transformedBody = transformedBody.replace(
                    'href="/coe21.css',
                    'href="https://web21.ufcoe.io/coe21.css'
                );
                return transformedBody;
            }
        }
    ]
}