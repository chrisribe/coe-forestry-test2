#!/usr/bin/env python3

import re
import os
import urllib.request
import hashlib
import fileinput

replacements = [
    {
        'file': "coe21.css",
        'search_re': "href=\"/coe21.css\?v=(\w+)\""        
    },
    {
        'file': "components.js",
        'search_re': "src=\"/components.js\?v=(\w+)\""
    }
]

site_root = "site_root"
source_root = "https://web21.ufcoe.io/"


for r in replacements:
    r_url = source_root + r['file']
    with urllib.request.urlopen(r_url) as f:
        r_contents = f.read()
        m = hashlib.md5()
        m.update(r_contents)
        r['tag'] = m.hexdigest()[:5]
 

for path, dirs, files in os.walk(site_root):
    f_paths = list(map(lambda f: os.path.join(path, f), files))
    f_paths_filtered = list(f for f in f_paths if ".html" in f)
    if len(f_paths_filtered) == 0:
        continue
    with fileinput.FileInput(files=f_paths_filtered, inplace=True) as file:
        for line in file:
            for r in replacements:
                m = re.search(r['search_re'], line)
                if m:
                    line = line.replace(m.group(1), r['tag'])
            
            print(line, end='')            

